import sinon from "sinon";
import { expect } from "chai";
import { getRideById, getRides, postCreateRide } from "../../src/ride";

describe("RideHandlers", () => {
  describe("#getRides", () => {
    describe("getRides successfully", () => {
      it("should return http 200 with a list of rides", async () => {
        const req: any = {
          di: {
            repositories: {
              ride: {
                find: sinon.stub().returns([
                  {
                    rideID: 1,
                    startLat: 2,
                    startLong: 3,
                    endLat: 4,
                    endLong: 9,
                    riderName: "Hieu",
                    driverName: "Hieu",
                    driverVehicle: "Honda SH",
                    created: "2021-08-21 11:36:54",
                  },
                ]),
              },
            },
          },
          query: {},
        };
        const res: any = {
          json: sinon.spy(),
          status: sinon.stub().returns({ json: sinon.spy() }),
        };

        await getRides(req, res);
        expect(req.di.repositories.ride.find.calledOnce).to.be.true;
        expect(res.status.calledWith(200)).to.be.true;
      });
    });

    describe("getRides with limit 0 successfully", () => {
      it("should return http 200 with an empty list of rides", async () => {
        const req: any = {
          di: {
            repositories: {
              ride: {
                find: sinon.stub().returns([]),
              },
            },
          },
          query: {
            limit: 0,
            offset: 0,
          },
        };
        const res: any = {
          json: sinon.spy(),
          status: sinon.stub().returns({ json: sinon.spy() }),
        };

        await getRides(req, res);
        expect(req.di.repositories.ride.find.calledOnce).to.be.true;
        expect(res.status.calledWith(200)).to.be.true;
      });
    });

    describe("get rides but server error", () => {
      it("should return http 500 error", async () => {
        const req: any = {};
        const res: any = {
          json: sinon.spy(),
          status: sinon.stub().returns({ json: sinon.spy() }),
        };

        await getRides(req, res);
        expect(res.status.calledWith(500)).to.be.true;
      });
    });
  });

  describe("#getRideById", () => {
    describe("get ride by id successfully", () => {
      it("should return http 200 with a ride data", async () => {
        const req: any = {
          params: {
            id: 1,
          },
          di: {
            repositories: {
              ride: {
                getById: sinon.stub().returns({
                  rideID: 1,
                  startLat: 2,
                  startLong: 3,
                  endLat: 4,
                  endLong: 9,
                  riderName: "Hieu",
                  driverName: "Hieu",
                  driverVehicle: "Honda SH",
                  created: "2021-08-21 11:36:54",
                }),
              },
            },
          },
        };
        const res: any = {
          json: sinon.spy(),
          status: sinon.stub().returns({ json: sinon.spy() }),
        };

        await getRideById(req, res);
        expect(req.di.repositories.ride.getById.calledOnce).to.be.true;
        expect(res.status.calledWith(200)).to.be.true;
      });
    });

    describe("get ride by id but server error", () => {
      it("should return http 500 error", async () => {
        const req: any = {
          params: {
            id: 1,
          },
        };
        const res: any = {
          json: sinon.spy(),
          status: sinon.stub().returns({ json: sinon.spy() }),
        };

        await getRideById(req, res);
        expect(res.status.calledWith(500)).to.be.true;
      });
    });
  });

  describe("#postCreateRide", () => {
    describe("create ride successfully", () => {
      it("should return http 200 with ride data", async () => {
        const req: any = {
          body: {
            start_lat: 2,
            start_long: 3,
            end_lat: 4,
            end_long: 9,
            rider_name: "Hieu",
            driver_name: "Hieu",
            driver_vehicle: "Honda SH",
          },
          di: {
            repositories: {
              ride: {
                create: sinon.stub().returns(1),
              },
            },
          },
        };
        const res: any = {
          json: sinon.spy(),
          status: sinon.stub().returns({ json: sinon.spy() }),
        };

        await postCreateRide(req, res);
        expect(req.di.repositories.ride.create.calledOnce).to.be.true;
        expect(res.status.calledWith(200)).to.be.true;
      });
    });

    describe("create ride but server error", () => {
      it("should return http 500 error", async () => {
        const req: any = {
          body: {
            start_lat: 2,
            start_long: 3,
            end_lat: 4,
            end_long: 9,
            rider_name: "Hieu",
            driver_name: "Hieu",
            driver_vehicle: "Honda SH",
          },
        };
        const res: any = {
          json: sinon.spy(),
          status: sinon.stub().returns({ json: sinon.spy() }),
        };

        await postCreateRide(req, res);
        expect(res.status.calledWith(500)).to.be.true;
      });
    });
  });
});
