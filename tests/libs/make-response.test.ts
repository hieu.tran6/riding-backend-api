import sinon from "sinon";
import { expect } from "chai";
import { makeResponse } from "../../src/libs/make-response";

describe("test makeResponse", () => {
  it("should response data with a corresponding status code", (done) => {
    const res: any = {
      json: sinon.spy(),
      status: sinon.stub().returns({ json: sinon.spy() }),
    };
    const sampleData = 1;
    makeResponse(res, 200, sampleData, null);
    expect(res.status.calledWith(200)).to.be.true;
    done();
  });
});
