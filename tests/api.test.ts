import { Express } from "express";
import { getExpressApp, runServer } from "../src/app";
import request from "supertest";
import { expect } from "chai";

describe("API tests", () => {
  let app: Express;
  before(async () => {
    await runServer();
    app = getExpressApp();
  });

  describe("GET /health", () => {
    it("should return health", (done) => {
      request(app)
        .get("/health")
        .expect("Content-Type", /text/)
        .expect(200, done);
    });
  });

  describe("POST /rides", () => {
    describe("success cases", () => {
      it("should create a ride success", (done) => {
        request(app)
          .post("/rides")
          .send({
            start_lat: 5,
            start_long: 5,
            end_lat: 5,
            end_long: 5,
            rider_name: "Hieu",
            driver_name: "Tran",
            driver_vehicle: "Honda Dream",
          })
          .then((res) => {
            expect(res.body.statusCode).equal(200);
            expect(res.body.data.rideID).not.to.be.undefined;
            expect(res.body.error).to.be.undefined;
            done();
          });
      });

      it("should create a ride success when start_lat is 90", (done) => {
        request(app)
          .post("/rides")
          .send({
            start_lat: 90,
            start_long: 5,
            end_lat: 5,
            end_long: 5,
            rider_name: "Hieu",
            driver_name: "Tran",
            driver_vehicle: "Honda Dream",
          })
          .then((res) => {
            expect(res.body.statusCode).equal(200);
            expect(res.body.data.rideID).not.to.be.undefined;
            expect(res.body.error).to.be.undefined;
            done();
          });
      });

      it("should create a ride success when start_lat is -90", (done) => {
        request(app)
          .post("/rides")
          .send({
            start_lat: -90,
            start_long: 5,
            end_lat: 5,
            end_long: 5,
            rider_name: "Hieu",
            driver_name: "Tran",
            driver_vehicle: "Honda Dream",
          })
          .then((res) => {
            expect(res.body.statusCode).equal(200);
            expect(res.body.data.rideID).not.to.be.undefined;
            expect(res.body.error).to.be.undefined;
            done();
          });
      });

      it("should create a ride success when start_long is -180", (done) => {
        request(app)
          .post("/rides")
          .send({
            start_lat: 5,
            start_long: 180,
            end_lat: 5,
            end_long: 5,
            rider_name: "Hieu",
            driver_name: "Tran",
            driver_vehicle: "Honda Dream",
          })
          .then((res) => {
            expect(res.body.statusCode).equal(200);
            expect(res.body.data.rideID).not.to.be.undefined;
            expect(res.body.error).to.be.undefined;
            done();
          });
      });

      it("should create a ride success when start_long is -180", (done) => {
        request(app)
          .post("/rides")
          .send({
            start_lat: 5,
            start_long: -180,
            end_lat: 5,
            end_long: 5,
            rider_name: "Hieu",
            driver_name: "Tran",
            driver_vehicle: "Honda Dream",
          })
          .then((res) => {
            expect(res.body.statusCode).equal(200);
            expect(res.body.data.rideID).not.to.be.undefined;
            expect(res.body.error).to.be.undefined;
            done();
          });
      });

      it("should create a ride success when end_lat is -90", (done) => {
        request(app)
          .post("/rides")
          .send({
            start_lat: 5,
            start_long: 5,
            end_lat: -90,
            end_long: 5,
            rider_name: "Hieu",
            driver_name: "Tran",
            driver_vehicle: "Honda Dream",
          })
          .then((res) => {
            expect(res.body.statusCode).equal(200);
            expect(res.body.data.rideID).not.to.be.undefined;
            expect(res.body.error).to.be.undefined;
            done();
          });
      });

      it("should create a ride success when end_lat is 90", (done) => {
        request(app)
          .post("/rides")
          .send({
            start_lat: 5,
            start_long: 5,
            end_lat: 90,
            end_long: 5,
            rider_name: "Hieu",
            driver_name: "Tran",
            driver_vehicle: "Honda Dream",
          })
          .then((res) => {
            expect(res.body.statusCode).equal(200);
            expect(res.body.data.rideID).not.to.be.undefined;
            expect(res.body.error).to.be.undefined;
            done();
          });
      });
    });
    describe("fail to validate body cases", () => {
      it("should response 422 error when start start_lat is -91", (done) => {
        request(app)
          .post("/rides")
          .send({
            start_lat: -91,
            start_long: 5,
            end_lat: 5,
            end_long: 5,
            rider_name: "Hieu",
            driver_name: "Tran",
            driver_vehicle: "Honda Dream",
          })
          .then((res) => {
            expect(res.statusCode).equal(422);
            expect(res.body.data).to.be.undefined;
            expect(res.body.errors.length).greaterThanOrEqual(1);
            done();
          });
      });

      it("should response 422 error when start start_long is -191", (done) => {
        request(app)
          .post("/rides")
          .send({
            start_lat: 5,
            start_long: -191,
            end_lat: 5,
            end_long: 5,
            rider_name: "Hieu",
            driver_name: "Tran",
            driver_vehicle: "Honda Dream",
          })
          .then((res) => {
            expect(res.statusCode).equal(422);
            expect(res.body.data).to.be.undefined;
            expect(res.body.errors.length).greaterThanOrEqual(1);
            done();
          });
      });

      it("should response 422 error when start end_lat is -91", (done) => {
        request(app)
          .post("/rides")
          .send({
            start_lat: 5,
            start_long: 5,
            end_lat: -91,
            end_long: 5,
            rider_name: "Hieu",
            driver_name: "Tran",
            driver_vehicle: "Honda Dream",
          })
          .then((res) => {
            expect(res.statusCode).equal(422);
            expect(res.body.data).to.be.undefined;
            expect(res.body.errors.length).greaterThanOrEqual(1);
            done();
          });
      });

      it("should response 422 error when end_long is -191", (done) => {
        request(app)
          .post("/rides")
          .send({
            start_lat: 5,
            start_long: 5,
            end_lat: 5,
            end_long: -191,
            rider_name: "Hieu",
            driver_name: "Tran",
            driver_vehicle: "Honda Dream",
          })
          .then((res) => {
            expect(res.statusCode).equal(422);
            expect(res.body.data).to.be.undefined;
            expect(res.body.errors.length).greaterThanOrEqual(1);
            done();
          });
      });

      it("should response 422 error when rider_name is empty", (done) => {
        request(app)
          .post("/rides")
          .send({
            start_lat: 5,
            start_long: 4,
            end_lat: 5,
            end_long: 5,
            rider_name: "",
            driver_name: "Tran",
            driver_vehicle: "Honda Dream",
          })
          .then((res) => {
            expect(res.statusCode).equal(422);
            expect(res.body.data).to.be.undefined;
            expect(res.body.errors.length).greaterThanOrEqual(1);
            done();
          });
      });

      it("should response 422 error when driver_name is empty", (done) => {
        request(app)
          .post("/rides")
          .send({
            start_lat: 5,
            start_long: 4,
            end_lat: 5,
            end_long: 5,
            rider_name: "Hieu",
            driver_name: "",
            driver_vehicle: "Honda Dream",
          })
          .then((res) => {
            expect(res.statusCode).equal(422);
            expect(res.body.data).to.be.undefined;
            expect(res.body.errors.length).greaterThanOrEqual(1);
            done();
          });
      });

      it("should response 422 error when driver_vehicle is empty", (done) => {
        request(app)
          .post("/rides")
          .send({
            start_lat: 5,
            start_long: 4,
            end_lat: 5,
            end_long: 5,
            rider_name: "Hieu",
            driver_name: "Trang",
            driver_vehicle: "",
          })
          .then((res) => {
            expect(res.statusCode).equal(422);
            expect(res.body.data).to.be.undefined;
            expect(res.body.errors.length).greaterThanOrEqual(1);
            done();
          });
      });
    });
  });

  describe("GET /rides", () => {
    before(async () => {
      await Promise.all(
        Array.from({ length: 20 }).map(() =>
          request(app)
            .post("/rides")
            .send({
              start_lat: Math.floor(Math.random() * 90),
              start_long: Math.floor(Math.random() * 180),
              end_lat: Math.floor(Math.random() * 90),
              end_long: Math.floor(Math.random() * 180),
              rider_name: (Math.random() + 1).toString(36).substring(7),
              driver_name: (Math.random() + 1).toString(36).substring(7),
              driver_vehicle: (Math.random() + 1).toString(36).substring(7),
            })
        )
      );
    });

    it("should return rides", (done) => {
      request(app)
        .get("/rides")
        .expect(200)
        .then((res) => {
          expect(res.body.statusCode).equal(200);
          expect(Array.isArray(res.body.data)).to.be.true;
          expect(res.body.error).to.be.undefined;
          done();
        });
    });

    it("should return top first 5 rides", (done) => {
      request(app)
        .get("/rides")
        .query({ limit: 5 })

        .expect(200)
        .then((res) => {
          expect(res.body.statusCode).equal(200);
          expect(Array.isArray(res.body.data)).to.be.true;
          expect(res.body.data.length).equal(5);
          expect(res.body.error).to.be.undefined;
          done();
        });
    });

    it("should return an empty array when offset is greater than the maximum items, here we assume that there are no more than 5000 rides", (done) => {
      request(app)
        .get("/rides")
        .query({ offset: 5000 })
        .expect(200)
        .then((res) => {
          expect(res.body.statusCode).equal(200);
          expect(Array.isArray(res.body.data)).to.be.true;
          expect(res.body.data.length).equal(0);
          expect(res.body.error).to.be.undefined;
          done();
        });
    });
  });
});
