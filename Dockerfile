FROM node:14-alpine as development

WORKDIR /app

RUN apk add python g++ make

COPY package.json yarn.lock tsconfig.json ./
RUN yarn install && yarn cache clean

COPY src ./src
RUN yarn build

FROM node:14-alpine as production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /app

RUN apk add python g++ make
COPY package.json yarn.lock ./
RUN yarn && yarn cache clean

COPY --from=development /app/dist /app/dist

USER node
CMD ["node", "/app/dist/index.js"]
