import express, { Express } from "express";
import { Server } from "http";
import swaggerUi from "swagger-ui-express";

import { closeDb, initializeDb } from "./db/db";
import { logger } from "./libs/logger";
import { injectDIContainer } from "./libs/middleware";
import { createRouter } from "./router";
import { Undefinable } from "./types";
import swaggerDocument from "./swagger.json";

const app = express();
app.use(express.json());

const PORT = 8010;

let server: Undefinable<Server>;

export async function runServer(): Promise<void> {
  const db = await initializeDb();
  app.use(injectDIContainer({ db }));
  createRouter(app);
  server = app.listen(PORT, () => {
    logger.info(`App started and listening on port ${PORT}`);
  });
}

export const getExpressApp = (): Express => app;

app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// Handle graceful shutdown
const gracefulShutdown = async () => {
  logger.info("Closing Database");
  await closeDb();
  logger.info("Database closed");
  if (server) {
    logger.info("Closing HTTP server");
    server.close(() => {
      logger.info("HTTP server closed");
    });
  }
};

process.once("SIGTERM", gracefulShutdown);
process.once("SIGINT", gracefulShutdown);
process.once("uncaughtException", gracefulShutdown);
