import { Request, Response } from "express";

export const getHealth = (req: Request, res: Response): void => {
  res.send("Healthy");
};
