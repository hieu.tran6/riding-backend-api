import { DataSourceAdapter } from "./data-source-adapter";

export interface Repository<T> {
  create(data: T): Promise<number>;
  getById(id: number): Promise<T>;
  find(limit: number, offset: number): Promise<T[]>;
}

export class BaseRepository<T> implements Repository<T> {
  private readonly dataSource: DataSourceAdapter<T>;

  constructor(dataSource: DataSourceAdapter<T>) {
    this.dataSource = dataSource;
  }

  create(data: T): Promise<number> {
    return this.dataSource.create(data);
  }
  getById(id: number): Promise<T> {
    return this.dataSource.getById(id);
  }
  find(limit?: number, offset?: number): Promise<T[]> {
    return this.dataSource.find(limit, offset);
  }
}
