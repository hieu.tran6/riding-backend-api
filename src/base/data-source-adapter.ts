import { Undefinable } from "../types";

export interface DataSourceAdapter<T> {
  find(limit: number, offset: number): Promise<T[]>;
  getById(id: number): Promise<Undefinable<T>>;
  create(data: T): Promise<number>;
}
