import { Express } from "express";
import { checkSchema, param, query } from "express-validator";
import { getHealth } from "./health";
import { checkValidationResult } from "./libs/middleware";
import { getRideById, getRides, postCreateRide } from "./ride";

export function createRouter(app: Express): void {
  app.get("/health", getHealth);
  app.get(
    "/rides",
    query("limit").isInt().toInt().optional(),
    query("offset").isInt().toInt().optional(),
    checkValidationResult,
    getRides
  );
  app.get(
    "/rides/:id",
    param("id").isInt().toInt(),
    checkValidationResult,
    getRideById
  );
  app.post(
    "/rides",
    checkSchema({
      start_lat: {
        isFloat: {
          options: {
            max: 90,
            min: -90,
          },
          errorMessage:
            "Start latitude must be between -90 - 90 degrees respectively",
        },
        toFloat: true,
      },
      start_long: {
        isFloat: {
          options: {
            max: 180,
            min: -180,
          },
          errorMessage:
            "Start latitude must be between -180 - 180 degrees respectively",
        },
        toFloat: true,
      },
      end_lat: {
        isFloat: {
          options: {
            max: 90,
            min: -90,
          },
          errorMessage:
            "End latitude must be between -90 - 90 degrees respectively",
        },
        toFloat: true,
      },
      end_long: {
        isFloat: {
          options: {
            max: 180,
            min: -180,
          },
          errorMessage:
            "End latitude must be between -180 - 180 degrees respectively",
        },
        toFloat: true,
      },
      rider_name: {
        isLength: {
          options: {
            min: 1,
          },
        },
        errorMessage: "Rider name must be a non empty string",
      },
      driver_name: {
        isLength: {
          options: {
            min: 1,
          },
        },
        errorMessage: "Driver name must be a non empty string",
      },
      driver_vehicle: {
        isLength: {
          options: {
            min: 1,
          },
        },
        errorMessage: "Driver vehicle must be a non empty string",
      },
    }),
    checkValidationResult,
    postCreateRide
  );
}
